defmodule OpinionatedQuotesApi.QuoteAPI.QuoteAPI do
  @moduledoc """
  The QuoteAPI context.
  """

  import Ecto.Query, warn: false
  alias OpinionatedQuotesApi.QuoteAPI.Quote
  alias OpinionatedQuotesApi.Repo

  @quote_cnt_cache_name :quote_cnt
  @quote_cnt_cache_max_age_MS 25*60*1_000 # 25 min

  @doc """
  Returns the list of quotes.

  ## Examples

      iex> list_quotes()
      [%Quote{}, ...]

  """
  def list_quotes(args) do
    build_query(args)
    |> Repo.all()
  end

  def get_recent_quote_count do
    cached_quote_cnt = :ets.lookup(@quote_cnt_cache_name, :quote_cnt) |> hd |> elem(1)
    cnt_age = :ets.lookup(@quote_cnt_cache_name, :updated_at) |> hd |> elem(1)

    if (
      :os.system_time(:millisecond) - cnt_age > @quote_cnt_cache_max_age_MS
      or cached_quote_cnt <= 0
    ) do
      quote_cnt = count_quotes()

      if quote_cnt > 0 do
        :ets.insert(
          @quote_cnt_cache_name,
          [{:quote_cnt, quote_cnt}, {:updated_at, :os.system_time(:millisecond)}]
        )
      end

      quote_cnt

    else
      cached_quote_cnt
    end
  end

  defp build_query(args) do
    rand = args[:rand]
    n = args[:n]
    offset = args[:offset]
    author = args[:author]
    tags = args[:tags]
    tagmode = args[:tagmode]
    lang = args[:lang]

    where = build_where(author, lang)

    q = from(q in Quote,
      preload: [:tags],
      join: tag in assoc(q, :tags),
      select: %{q | tags: fragment("array_agg(?)", tag.name)},
      where: ^where,
      offset: ^offset,
      limit: ^n,
      group_by: q.id,
      having: fragment(
        "CASE WHEN ? ILIKE 'any' THEN ? && array_agg(?) ELSE ? <@ array_agg(?) END",
        ^tagmode,
        ^tags,
        tag.name,
        ^tags,
        tag.name
      )
    )

    if rand do
      # TODO: find a way to use faster TABLESAMPLE SYSTEM w/o raw query
      order_by(q, fragment("random()"))
    else
      q
    end
  end

  defp build_where(author, lang) do
    author = author && "%#{author}%"
    # https://hexdocs.pm/ecto/Ecto.Query.html#dynamic/2
    dynamic = false

    cond do
      author && lang ->
        dynamic([q], (ilike(q.author, ^author) and (is_nil(q.lang) or q.lang == "en")) or ^dynamic)
      author ->
        dynamic([q], ilike(q.author, ^author) or ^dynamic)
      lang ->
        dynamic([q], q.lang == ^lang or ^dynamic)
      :otherwise ->
        dynamic([q], (is_nil(q.lang) or q.lang == "en") or ^dynamic)
    end
  end

  defp count_quotes do
    Repo.aggregate(Quote, :count, :id)
  end
end
